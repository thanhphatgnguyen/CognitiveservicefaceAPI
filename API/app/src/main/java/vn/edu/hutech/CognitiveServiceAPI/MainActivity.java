package vn.edu.hutech.CognitiveServiceAPI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import java.io.*;
import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.graphics.*;
import android.provider.*;
import com.microsoft.projectoxford.face.*;
import com.microsoft.projectoxford.face.contract.*;

public class MainActivity extends AppCompatActivity {

    ImageView imgHienthi;
    Button btnNhandien,btnChonhinh,btnChuphinh;
    private final int RQ_TAKE_PHOTO=123;
    private final int RQ_CHOOSE_PHOTO=321;
    private ProgressDialog detectionProgressDialog;
    private FaceServiceClient faceServiceClient =
            new FaceServiceRestClient("d24ac2efdc3f41a0a38639c2a2d058cd");
    Bitmap myBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControl();
        addEvent();
    }

    private void addControl() {
        imgHienthi =(ImageView) findViewById(R.id.imgHienthi);
        btnNhandien=(Button) findViewById(R.id.btnNhandien);
        btnChonhinh=(Button)findViewById(R.id.btnChonhinh);
        btnChuphinh=(Button)findViewById(R.id.btnChuphinh);
    }

    private void addEvent() {

        btnNhandien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BitmapDrawable bmd=(BitmapDrawable)imgHienthi.getDrawable();
                myBitmap=bmd.getBitmap();
                detectionProgressDialog = new ProgressDialog(MainActivity.this);
                detectAndFrame(myBitmap);
            }
        });
        btnChonhinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Chonhinh();
            }
        });
        btnChuphinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Chuphinh();
            }
        });
    }

    private void Chuphinh() {
        Intent it=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(it,RQ_TAKE_PHOTO);
    }

    private void Chonhinh() {
        Intent it=new Intent(Intent.ACTION_PICK);
        it.setType("image/*");
        startActivityForResult(it,RQ_CHOOSE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==RQ_CHOOSE_PHOTO &&resultCode==RESULT_OK)
        {


            try {
                Uri imageURI=data.getData();
                InputStream is= getContentResolver().openInputStream(imageURI);
                Bitmap bm=BitmapFactory.decodeStream(is);
                imgHienthi.setImageBitmap(bm);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
        else
        {
            if(requestCode==RQ_TAKE_PHOTO&&resultCode==RESULT_OK)
            {
                Bitmap bm=(Bitmap) data.getExtras().get("data");
                imgHienthi.setImageBitmap(bm);
            }
        }
    }

    private void detectAndFrame(final Bitmap imageBitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
        ByteArrayInputStream inputStream =
                new ByteArrayInputStream(outputStream.toByteArray());
        AsyncTask<InputStream, String, Face[]> detectTask =
                new AsyncTask<InputStream, String, Face[]>() {
                    @Override
                    protected Face[] doInBackground(InputStream... params) {
                        try {
                            publishProgress("Detecting...");
                            Face[] result = faceServiceClient.detect(
                                    params[0],
                                    true,         // returnFaceId
                                    false,        // returnFaceLandmarks
                                    null           // returnFaceAttributes: a string like "age, gender"
                            );
                            if (result == null)
                            {
                                publishProgress("Detection Finished. Nothing detected");
                                return null;
                            }
                            publishProgress(
                                    String.format("Detection Finished. %d face(s) detected", result.length));
                            return result;
                        } catch (Exception e) {
                            publishProgress("Detection failed");
                            return null;
                        }
                    }
                    @Override
                    protected void onPreExecute() {

                        detectionProgressDialog.show();
                    }
                    @Override
                    protected void onProgressUpdate(String... progress) {

                        detectionProgressDialog.setMessage(progress[0]);
                    }
                    @Override
                    protected void onPostExecute(Face[] result) {

                        detectionProgressDialog.dismiss();
                        if (result == null) return;
                        imgHienthi.setImageBitmap(drawFaceRectanglesOnBitmap(imageBitmap, result));
                        imageBitmap.recycle();
                    }
                };
        detectTask.execute(inputStream);
    }
    private static Bitmap drawFaceRectanglesOnBitmap(Bitmap originalBitmap, Face[] faces) {
        Bitmap bitmap = originalBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        int stokeWidth = 2;
        paint.setStrokeWidth(stokeWidth);
        if (faces != null) {
            for (Face face : faces) {
                FaceRectangle faceRectangle = face.faceRectangle;
                canvas.drawRect(
                        faceRectangle.left,
                        faceRectangle.top,
                        faceRectangle.left + faceRectangle.width,
                        faceRectangle.top + faceRectangle.height,
                        paint);
            }
        }
        return bitmap;
    }
}
